from random import randint

name = input("Hi! What is your name? ")

for guess in range(5):

    birthmonth = randint(1,12)
    birthyear = randint(1924,2004)

    print("Guess", guess+1, ": ", name, "were you born in ", birthmonth, "/", birthyear,"?")

    result = input("yes or no? ")

    if result == "yes":
        print("I knew it!")
        exit()

    elif guess < 4:
        print("Drat! Lemme try again")

    else:
        print("I have other things to do. Good bye.")
